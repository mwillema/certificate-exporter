FROM ubuntu

RUN apt-get update && apt-get install -y \
    jq \
    libarchive-zip-perl \
    xxd

COPY export.sh /export.sh
RUN chmod +x /export.sh

CMD ["/export.sh"]
