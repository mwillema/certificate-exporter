# Certificate Exporter

In this challenge, we ask you to read the JSON file located in the `resources` directory and export all items to the `export` directory.

The latter must be structured as follow:

```
export
└── Device000000001
    ├── Device_Cert.p12
    ├── Device_id.txt
    ├── Device_id_CHK.txt
    └── NDEF.txt
```

Each tag data will be in its own subdirectory. This subdirectory is named `Device` followed by the number of the tag, beginning at one (1), in *hexadecimal*, and zero left-padded to a total size of eight positions. The number will be incremented by one (1) at each tag.

* `Device_Cert.p12`: the content of the `certificate_data` field;
* `Device_id.txt`: the `serial_number` field;
* `Device_id_CHK.txt`: the *hexadecimal* representation of the CRC32 of `serial_number` field;
* `NDEF.txt`: the *uppercase hexadecimal* representation of the `short_url_code` field. Each byte representation must be separated by a space (cf. hereafter);

A full example of exported data associated with a tag would be:

```json
   {
      "uuid":"6214d4da-1ca1-4fd6-ab9d-e2981a01e37f",
      "manufacturer_serial_number":null,
      "tag_order_uuid":"99241cc8-49e3-4aeb-b2c4-2f8f5a8d765d",
      "product_uuid":null,
      "product_model_uuid":null,
      "brand_uuid":"1c639c03-42bb-4484-ba79-537b2c6da690",
      "serial_number":"5E0000000000000032DE9F74280EB33B",
      "form_factor":null,
      "short_url_code":"jX-4NmWjBts",
      "short_url_code_open":"jX-4NmWjBts9JJ5XQ",
      "redirection_url":null,
      "redirection_url_open":null,
      "ndef":true,
      "open_detection":true,
      "open":false,
      "chipset_family":"VaultIC",
      "certificate_serial_number":"015E000000000000000000000000005F",
      "certificate_data":"-----BEGIN PKCS12-----\n...\n-----END PKCS12-----\n",
      "certificate_authority_uuid":"51b7abfd-4b38-4b8a-b3b3-188500dec700",
      "certificate_format":"PKCS12",
      "symmetric_key":null
   },

```

Suppose it is the 50th tag in the JSON file, the corresponding files within the `Device00000032` directory would contain

File name | Content
----------| -------
`Device_Cert.p12` | `-----BEGIN PKCS12-----\n...\n-----END PKCS12-----\n`
`Device_id.txt` | `5E0000000000000032DE9F74280EB33B`
`Device_id_CHK.txt` | `04C6C2B0`
`NDEF.txt` | `6A 58 2D 34 4E 6D 57 6A 42 74 73`

## Remarks

Don't optimize prematurely !
