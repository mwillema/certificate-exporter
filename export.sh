#!/bin/bash

set -e

##
# Main function of the script. Does something useful, hopefully ;-)
#
function main() {
    cd /export
    rm -rf *

    for tag in $(jq -r '.[] | @base64' /resources/certificates.json); do
        ((++tag_number))
        export_tag $tag $tag_number
    done
}

##
# Exports the tag number $2 from the JSON document $1 into its own directory.
#
# $1 is a JSON document encoded in base64 representing the tag to export
# $2 is a positive integer representing the number of the tag
#
function export_tag() {
    create_and_use_dir $(tag_directory_of $2)

    export_certificate_data $1
    export_serial_number $1
    export_crc32 $1
    export_ndef $1

    cd ..
}

##
# Prints to stdout the directory name of the tag number $1
#
# $1 is a positive integer representing the number of a tag
#
function tag_directory_of() {
    printf 'Device%08x' $1
}

##
# Writes the content of the certificate_data field found in the JSON document $1
# into the Device_Cert.p12 file in the current directory.
#
# $1 is a JSON document encoded in base64 containing the certificate_data field
#
function export_certificate_data() {
    echo $(json $1 '.certificate_data') > Device_Cert.p12
}

##
# Writes the serial_number field found in the JSON document $1 into the
# Device_id.txt file in the current directory.
#
# $1 is a JSON document encoded in base64 containing the serial_number field
#
function export_serial_number() {
    echo $(json $1 '.serial_number') > Device_id.txt
}

##
# Writes the hexadecimal representation of the CRC32 of the serial_number field
# found in the JSON document $1 into the Device_id_CHK.txt file in the current
# directory.
#
# $1 is a JSON document encoded in base64 containing the serial_number field
#
function export_crc32() {
    local serial_number=$(json $1 '.serial_number')
    crc32 <(echo $serial_number) > Device_id_CHK.txt
}

##
# Writes the uppercase hexadecimal representation of the short_url_code field
# found in the JSON document $1 into the NDEF.txt file in the current directory.
#
# $1 is a JSON document encoded in base64 containing the short_url_code field
#
function export_ndef() {
    local short_url_code=$(json $1 '.short_url_code')
    printf "%s\n" $short_url_code | xxd -g 1 -p -u | sed 's/.\{2\}/& /g' > NDEF.txt
}

##
# Executes the jq expression $2 in the JSON document $1
#
# $1 is a JSON document encoded in base64
# $2 is a jq expression matching a part of the decoded JSON document $1
#
function json() {
    echo $1 | base64 -d | jq -r $2
}

##
# Creates the directory $1 in the current one and sets $1 as the current directory.
#
# $1 names a non-existing directory to be created
#
function create_and_use_dir() {
    mkdir $1
    cd $1
}


# ----------------------------
# Script starts here
# ----------------------------

main
